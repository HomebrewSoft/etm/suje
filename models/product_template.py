# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductTemplate(models.Model):
    _inherit = "product.template"

    categ_parent_id = fields.Many2one(
        related="categ_id.parent_id",
    )
    unique_code = fields.Char()
    cut_type = fields.Selection(
        selection=[
            ("half_cut", _("Half Cut")),
            ("full_cut", _("Full cut")),
            ("perforated", _("Perforated")),
        ],
    )
    max_with = fields.Float(
        string="Ancho Máximo",
    )
    standard_with = fields.Float(
        string="Ancho Estándar",
    )
    best_with = fields.Float(
        string="Ancho optimo",
    )
    press_code = fields.Char()
    # suje_description = fields.Text(  # TODO
    # )
    axis = fields.Float()
    development = fields.Float()
    cavity_axis = fields.Float()
    cavity_development = fields.Float()
    separation_axis = fields.Float()
    separation_development = fields.Float()
    material_width = fields.Float()
    status = fields.Selection(
        selection=[
            ("active", _("Active")),
            ("inactive", _("Inactive")),
            ("repair", _("Repair")),
            ("in_transit", _("In transit")),
        ],
    )
    cut_shape = fields.Selection(
        selection=[
            ("R", "R"),
            ("I", "I"),
            ("C", "C"),
            ("CR", "CR"),
            ("O", "O"),
            ("RF", "RF"),
        ],
    )
    corner = fields.Selection(
        selection=[
            ("1\8_std", _("1\8 (STD)")),
            ("recto", _("90° recto")),
            ("1_64", "1\ 64"),
            ("1_32", "1\ 32"),
            ("1_16", "1\ 16"),
            ("3_32", "3\ 32"),
            ("1_4", "1\ 4"),
        ],
        string="Tipo de Esquina"
    )
    date_purchase = fields.Date()
    notes = fields.Text()
    piece_type = fields.Selection(
        selection=[
            ("suje", _("Suje")),
            ("cutter", _("Cutter")),
            ("removable_perforator", _("Removable perforator")),
            ("removable_perforator_jojador", _("Jojador removable perforator")),
            ("removable_half_cut", _("Removable half cut")),
        ],
    )
    gear = fields.Char()
    no_rollers = fields.Integer(
        string="No. Rollers",
    )
    location = fields.Char()
    categ_ids = fields.Many2many(
        comodel_name="product.category",
    )
