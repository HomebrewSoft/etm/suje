# -*- coding: utf-8 -*-
{
    'name': 'Suje',
    'version': '13.0.0.3.1',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'product',
    ],
    "data": [
        # security
        # data
        "data/product_category.xml",
        # reports
        # views
        "views/product_template.xml",
    ],
}
